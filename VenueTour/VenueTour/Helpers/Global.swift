//
//  Global.swift
//  Invhite
//
//  Created by Kmphasis on 31/12/18.
//  Copyright © 2018 Kmphasis. All rights reserved.
//

import Foundation
import UIKit


enum Storyboard : String {
    
    case Main = "Main"
    
}

let LATITUDE = "latitude"
let LONGITUDE = "longitude"

//MARK: - Constants

class Constants {
    
    // Define all constants here, start with small-k
    
    static let kLoginType = "loginType" // Type:0 <- Via Phone Number || Type:1 <- Via Email ID
    static let kLoginCount = "loginCount"
    static let kDeviceType = "Device"
    
    static let kEmail = "Email"
    static let kName = "Name"
    static let kGender = "Gender"
    static let kToken = "Token"
    
    static let kUserModel = "UserModel"
    
    static let kUserId = "UserID"
    static let kFname = "Fname"
    static let kLname = "Lname"
    static let kPhone = "Phone"
    static let kAddress = "Address"
    static let kProfile = "Profile"
    static let kIsRegister = "IsRegister"
    static let kUserType = "kusertype"
    
    static let kFacebook = "Facebook"
    static let kGoogle = "Google"
    static let kTwitter = "Twitter"
    static let kSocialType = "Type"
    
    static let kIsConfirmationLink = "ConfirmationLink"
    
    static let kLATITUDE = "LATITUDE"
    static let kLONGITUDE = "LONGITUDE"
    
    static let kCity = "City"
    static let kZip = "Zip"
    
    static var kCurrentStoryboard = Storyboard.Main {
        didSet {
            userDefault.setValue(kCurrentStoryboard.rawValue, forKey: "CurrentStoryboard")
            userDefault.synchronize()
        }
    }
    
    static var currentStoryboard : UIStoryboard {
        get {
            if let current = userDefault.value(forKey: "CurrentStoryboard") as? String {
                return UIStoryboard(name: current, bundle: Bundle.main)
            }
            else {
                return UIStoryboard(name: "Main", bundle: Bundle.main)
            }
        }
    }
    
//    private static var setUser : User?
//    static var currentUser : User? {
//        get {
//            if setUser == nil {
//                if let usermodel = userDefault.value(forKey: Constants.kUserModel) {
//                    let object = try! JSONDecoder().decode(User.self, from: usermodel as! Data)
//                    userDefault.setValue(object.token!, forKey: Constants.kToken)
//                    setUser = object
//                }
//            }
//            return setUser
//        }
//        set {
//            if newValue != nil {
//                let userdata = try! JSONEncoder().encode(newValue!)
//                userDefault.setValue(userdata, forKey: Constants.kUserModel)
//                userDefault.setValue(newValue!.token!, forKey: Constants.kToken)
//                setUser = newValue
//            }
//            else {
//                setUser = nil
//            }
//        }
//    }
}


//*********************************
//MARK:-
//MARK:-   API Urls
//*********************************
struct Urls {

    private static let BasicUrl = "https://www.beemia.com/Api/"
    //private static let BasicUrl = "http://3.18.37.158/Api/"
    
    struct Login {
        static let LoginAPI = BasicUrl + "User/Login"
    }
    
    struct Signup {
        static let forSignup = BasicUrl + "Signup/forSignup"
        static let SignupAPI = BasicUrl + "User/Signup"
        static let SignupOtherAPI = BasicUrl + "User/signUp_other"
        static let getOrganization = BasicUrl + "Signup/getOrganization"
        static let getEventTag = BasicUrl + "Signup/getEventTag"
        static let forgotPassword = BasicUrl + "User/forgotPassword"
    }
    
    struct appFlow {

        // feed
        static let getUserFeed = BasicUrl + "Signup/getUserFeed"
        
        // Apartment
        static let getApartment = BasicUrl + "Signup/getApartment"
        
        // Sublet
        static let getSublet = BasicUrl + "Signup/getSublet"
        static let addSublet = BasicUrl + "Add/addSublet"
        
        // Band
        static let getBand = BasicUrl + "Signup/getBand"
        static let getBandSchedual = BasicUrl + "Special/getBandSchedual"
        
        // bar
        static let getBar = BasicUrl + "Signup/getBar"
        static let getHours = BasicUrl + "Special/getHours"
        static let getBarSpecial = BasicUrl + "Special/getBarSpecial"
        static let getBarMusicBand = BasicUrl + "Special/getBarBand"
        static let getBarGuide = BasicUrl + "Signup/getBarGuide"

        // Events
        static let getEvent = BasicUrl + "Signup/getEvent"
        static let addEvent = BasicUrl + "Add/addEvent"
        
        // Buzz
        static let getBuzz = BasicUrl + "Signup/getBuzz"
        
        static let addFavURL = BasicUrl + "Signup/addFavourite"
        
        // Profile
        static let UpdateProfile = BasicUrl + "User/updateProfile"
        
        // Feedback
        static let Feedback = BasicUrl + "Signup/sendFeedback"
    }
    
}

//*********************************
//MARK:-
//MARK:-   RegisterData Dictionary
//*********************************

var finalRegisterDataDic = NSMutableDictionary()
var userType = ""

struct TextfieldKeys {
    
    static let emailAddress = "emailAddress"
    static let password = "password"
    static let phoneNumber = "phoneNumber"
    static let fname = "fname"
    static let lname = "lname"
    static let country = "country"
    static let addresss = "addresss"
    static let citizenTaxNumber = "citizenTaxNumber"
    static let idNumber = "idNumber"
    static let passportNumber = "passportNumber"
    static let wareHouseLocationc = "wareHouseLocationc"
    static let sizeOfSpace = "sizeOfSpace"
    static let contractulLink = "contractulLink"
    static let profilePic = "profilePic"
    static let businessName = "businessName"
    static let businessLogo = "businessLogo"
    
}

struct Strings {
    
    static let kOK = "OK"
    static let kNo = "No"
    static let kEnterPwd = "Please enter valid password."
    static let kChoosseCamera = "Choose Image"
    static let kCamera = "Camera"
    static let kGallery = "Gallery"
    static let kCancel = "Cancel"
    static let kWarning = "Warning"
    static let kCameraWarningMsg = "You don't have camera"
    static let kType = "Please Select Type"
    static let kUser = "User"
    static let kOwner = "Owner"
    static let kDriver = "Driver"
    static let kPasswordAlert = "Please enter password"
    static let kValidEmail = "Please enter valid email address"
    static let kEmailAlert = "Please enter email address"
    static let kNameAlert = "Please enter name"
    static let kSubjectAlert = "Please enter subject."
    static let kMsgAlert = "Please enter message."
    static let kDeviceType = "iOS"
    static let kUserType = "Buyer"
    
}


//var forSignUp : ForSignUp?
//func getForSignup(_ completion: @escaping ((_ success: Bool)->())) {
//    AFClass.postRequest(with: Urls.Signup.forSignup, params: [:], headerKeys: [], isKey: true, entity: ForSignUp.self, isShowHud: true) { (response, resMsg, resCode, resError) in
//
//        guard resError == nil else {
//            appdelegate.mainNav!.errorAlert(message: resError ?? "")
//            completion(false)
//            return
//        }
//
//        if let signUp = response as? [String: Any], let model = ForSignUp(dict: signUp) {
//            forSignUp = model
//            completion(true)
//        }
//    }
//}
