//
//  DropShadowView.swift
//  Beemia
//
//  Created by Mac Os on 31/01/19.
//  Copyright © 2019 Kmphasis. All rights reserved.
//

import UIKit


class DropShadowView: UIView {
    
    @IBInspectable var dropCornerRadius: CGFloat = 5.0 {
        didSet {
            self.updateProperties()
        }
    }
    
    @IBInspectable var dropShadowColor: UIColor = UIColor("54757A") {
        didSet {
            self.updateProperties()
        }
    }

    @IBInspectable var dropShadowOffset: CGSize = CGSize(width: 0.0, height: 0.0) {
        didSet {
            self.updateProperties()
        }
    }

    @IBInspectable var dropShadowRadius: CGFloat = 3.0 {
        didSet {
            self.updateProperties()
        }
    }
    
    @IBInspectable var dropShadowOpacity: Float = 0.18 {
        didSet {
            self.updateProperties()
        }
    }
    
    /**
     Masks the layer to it's bounds and updates the layer properties and shadow path.
     */
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.masksToBounds = false
        
        self.updateProperties()
        self.updateShadowPath()
    }
    
    /**
     Updates all layer properties according to the public properties of the `ShadowView`.
     */
    fileprivate func updateProperties() {
        self.layer.cornerRadius = self.dropCornerRadius
        self.layer.shadowColor = self.dropShadowColor.cgColor
        self.layer.shadowOffset = self.dropShadowOffset
        self.layer.shadowRadius = self.dropShadowRadius
        self.layer.shadowOpacity = self.dropShadowOpacity
    }
    
    /**
     Updates the bezier path of the shadow to be the same as the layer's bounds, taking the layer's corner radius into account.
     */
    fileprivate func updateShadowPath() {
        self.layer.shadowPath = UIBezierPath(roundedRect: layer.bounds, cornerRadius: layer.cornerRadius).cgPath
    }
    
    /**
     Updates the shadow path everytime the views frame changes.
     */
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.updateShadowPath()
    }
}


