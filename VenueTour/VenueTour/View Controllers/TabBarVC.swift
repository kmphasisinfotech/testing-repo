//
//  TabBarVC.swift
//  VenueTour
//
//  Created by Mayank on 30/03/19.
//  Copyright © 2019 Mayank. All rights reserved.
//


import Foundation
import UIKit

class TabBarVC: UIViewController {
    
    //***********************************************
    //MARK:-
    //MARK:-   Outlets
    //***********************************************
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var tabBtns: [UIButton]!
    
    
    //***********************************************
    //MARK:-
    //MARK:-   Other Properties
    //***********************************************
    var vcArr = [UIViewController]()
    var pageViewController : UIPageViewController!
    var selectBtnIndex = 0
    
    
    //***********************************************
    //MARK:-
    //MARK:-  VC Life Cycle
    //***********************************************
    override func viewDidLoad() {
        super.viewDidLoad()

        selectBtnIndex = 0
        
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        addChildVC()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func addChildVC() {
        
        vcArr = [UIStoryboard.instantiateVC(ZooViewVC.self)] //, UIStoryboard.instantiateVC(ZooViewVC.self), UIStoryboard.instantiateVC(ZooViewVC.self)]
        
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)
        containerView.addSubview(pageViewController.view)
        
        setPageControllerConstraint()
        pageViewController.setViewControllers([vcArr.first!], direction: .forward, animated: false, completion: nil)
    }
    
    func setPageControllerConstraint() {
        
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: pageViewController.view, attribute: .top, relatedBy: .equal, toItem: containerView, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: pageViewController.view, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: pageViewController.view, attribute: .right, relatedBy: .equal, toItem: containerView, attribute: .right, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: pageViewController.view, attribute: .left, relatedBy: .equal, toItem: containerView, attribute: .left, multiplier: 1.0, constant: 0.0).isActive = true
    }
}


//***********************************************
//MARK:-
//MARK:-   IBActions
//***********************************************
extension TabBarVC {
    
//    @IBAction func menu_Action(_ sender: UIButton) {
//
//        if let drawer = navigationController!.parent as? KYDrawerController {
//            drawer.setDrawerState(.opened, animated: true)
//        }
//    }
    
    @IBAction func tabBtns_Action(_ sender: UIButton) {
        if sender.tag == 0 {
          setVC(sender.tag)
        }
    }
}



//***********************************************
//MARK:-
//MARK:-   UICollectionView DataSource, Delegate
//***********************************************
extension TabBarVC : UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let vcindex = vcArr.index(of: viewController) else { return nil }
        let previousIndex = vcindex - 1
        guard previousIndex >= 0 else { return nil }
        guard vcArr.count > previousIndex else { return nil }
        return vcArr[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let vcindex = vcArr.index(of: viewController) else { return nil }
        let nextIndex = vcindex + 1
        guard vcArr.count != nextIndex else { return nil }
        guard vcArr.count > nextIndex else { return nil }
        return vcArr[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if !completed {
            return
        }
        guard let vc = pageViewController.viewControllers?.first else { return }
        guard let vcindex = vcArr.index(of: vc) else { return }
        selectBtnIndex = vcindex
    }
    
    func setVC(_ index: Int) {
        
        let vc = vcArr[index]
        pageViewController.setViewControllers([vc], direction: (index > selectBtnIndex) ? .forward : .reverse, animated: true) { (success) in
            self.selectBtnIndex = index
        }
    }
}



