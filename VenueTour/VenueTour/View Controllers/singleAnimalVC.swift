//
//  singleAnimalVC.swift
//  VenueTour
//
//  Created by Mayank on 30/03/19.
//  Copyright © 2019 Mayank. All rights reserved.
//


import Foundation
import UIKit
import AVFoundation
import DropDown

class singleAnimalVC: UIViewController {
    
    //***********************************************
    //MARK:-
    //MARK:-   Outlets
    //***********************************************
    
    @IBOutlet weak var playPauseImg: UIImageView!
    
    @IBOutlet weak var durationSlider: UISlider!
    @IBOutlet weak var durationStartTime: UILabel!
    @IBOutlet weak var durationEndTime: UILabel!
    
    @IBOutlet weak var playerSliderView: UIView!
    @IBOutlet weak var playRateXLbl: UILabel!

    @IBOutlet weak var animalNameLbl: UILabel!
 
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var upBtn: UIButton!
    @IBOutlet weak var downBtn: UIButton!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    
    // PopUp
    @IBOutlet weak var popSettingsView: UIView!
    @IBOutlet weak var popMainSettingsView: UIView!
    
    @IBOutlet weak var langBtn: UIButton!
    @IBOutlet weak var langLbl: UILabel!
    @IBOutlet weak var flagImg: UIImageView!
    
    @IBOutlet weak var criteriaBtn: UIButton!
    @IBOutlet weak var selectCriteriaLbl: UILabel!

    //***********************************************
    //MARK:-
    //MARK:-   Other Properties
    //***********************************************
    
    var myPlayer = AVAudioPlayer()
    var playDuration : Float?
    let languageDropdown = DropDown()
    let narrativeDropdown = DropDown()
    
    var animalName = ""

    //***********************************************
    //MARK:-
    //MARK:-  VC Life Cycle
    //***********************************************
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.isHidden = true
        popSettingsView.isHidden = true
        
        let flagArr = [UIImage(named: "us"),UIImage(named: "spain"),UIImage(named: "china")]
        languageDropdown.dataSource = ["English", "Spanish", "Chinese"]
        languageDropdown.backgroundColor = UIColor("DFEBEE")
        languageDropdown.selectionBackgroundColor = UIColor("88B836")
        languageDropdown.width = langBtn.frame.width
        languageDropdown.textFont = UIFont(name: "Open Sans", size: 17)!
        languageDropdown.anchorView = langBtn
        languageDropdown.selectionAction = {[weak self](index, item) in
            self!.langLbl.text = item
            self!.flagImg.image = flagArr[index]
        }
        
        narrativeDropdown.dataSource = ["For Kids", "For Adults","Computer Generated"]
        narrativeDropdown.textFont = UIFont(name: "Open Sans", size: 17)!
        narrativeDropdown.backgroundColor = UIColor("DFEBEE")
        narrativeDropdown.selectionBackgroundColor = UIColor("88B836")
        narrativeDropdown.width = criteriaBtn.frame.width
        narrativeDropdown.anchorView = criteriaBtn
        narrativeDropdown.selectionAction = {[weak self](index, item) in
            self?.selectCriteriaLbl.text = item
        }
        
        animalNameLbl.text = animalName
        
        do {
            myPlayer = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "tigerRoar", ofType: ".mp3")!))
            myPlayer.prepareToPlay()
            
            playDuration = Float(myPlayer.duration)
            durationSlider.maximumValue = playDuration!
            
            let min:Int = (Int(playDuration!) % 3600 ) / 60
            let sec = (Int(playDuration!) % 3600 ) % 60
            
            print(myPlayer.duration)
            print("\(min):\(sec)")
            durationEndTime.text = String(min) + ":" + String(sec)
            
            myPlayer.enableRate = true
            myPlayer.play()
        }
        catch {
            print(error)
        }
        
        var timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
        myPlayer.rate = 1.0
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        textViewHeightConstraint.constant = textView.Getheight
    }
    
    @objc func updateSlider() {
        
        durationSlider.value = Float(myPlayer.currentTime)
        
        let time = myPlayer.currentTime
        
        let min:Int = (Int(time) % 3600 ) / 60
        let sec = (Int(time) % 3600 ) % 60
        
        durationStartTime.text = String(min) + ":" + String(sec)
    }
}



//***********************************************
//MARK:-
//MARK:-   IBActions
//***********************************************
extension singleAnimalVC {
    
    
    @IBAction func backBtn(_ sender: UIButton) {
       
       myPlayer.pause()
       backVC()
    }
    
    @IBAction func criteriaBtn(_ sender: UIButton) {
      
        self.narrativeDropdown.show()
    }
    
    @IBAction func langBtn(_ sender: UIButton) {
    
        self.languageDropdown.show()
    }
    
    @IBAction func closePopBtn(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.35, animations: {
            self.popMainSettingsView.transform = CGAffineTransform(translationX: 0, y: UIScreen.height)
        }) { (suc) in
            self.popSettingsView.isHidden = true
        }
    }
    
    @IBAction func downBtn(_ sender: UIButton) {
        
        textViewHeightConstraint.constant = textView.Getheight
        UIView.animate(withDuration: 0.35, animations: {
            self.view.layoutIfNeeded()
            //self.textView.transform = CGAffineTransform(translationX: 0, y: UIScreen.height)
        }) { (suc) in
            self.textView.isHidden = true
        }
    }
    
    @IBAction func upBtn(_ sender: UIButton) {
        textView.isHidden = false
        //textView.transform = CGAffineTransform(translationX: 0, y: playerSliderView.frame.maxY)
        textViewHeightConstraint.constant = 0
        UIView.animate(withDuration: 0.45) {
            //self.textView.transform = CGAffineTransform.identity
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func seekDuration(_ sender: UISlider) {
        
        myPlayer.stop()
        myPlayer.currentTime = TimeInterval(durationSlider.value)
        playPauseImg.image = UIImage(named: "push")
        myPlayer.prepareToPlay()
        myPlayer.play()
        
    }
    
    @IBAction func previousBtn_Action(_ sender: UIButton) {
        
        print("Previous")
        
        myPlayer.stop()
        myPlayer.currentTime -= 2
        myPlayer.prepareToPlay()
        myPlayer.play()
        
    }
    
    @IBAction func settingsBtn(_ sender: UIButton) {
        
        myPlayer.pause()
        
        popSettingsView.isHidden = false
        popMainSettingsView.transform = CGAffineTransform(translationX: 0, y: -UIScreen.height)
        UIView.animate(withDuration: 0.45) {
            self.popMainSettingsView.transform = CGAffineTransform.identity
        }
    }
    
    @IBAction func playPauseBtn_Action(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            playPauseImg.image = UIImage(named: "play")
            sender.tag = 0
            myPlayer.pause()
            print(sender.tag)
            
        }
            
        else {
            
            playPauseImg.image = UIImage(named: "push")
            sender.tag = 1
            myPlayer.play()
            print(sender.tag)
            
        }
        
    }
    
    @IBAction func nextBtn_Action(_ sender: UIButton) {
        
        print("Next")
        
        myPlayer.stop()
        myPlayer.currentTime += 2
        myPlayer.prepareToPlay()
        myPlayer.play()
        
    }
    
    @IBAction func rateBtn_Action(_ sender: UIButton) {
        
        if sender.tag == 1 {
            
            sender.tag = 2
            myPlayer.stop()
            myPlayer.rate = 1.5
            playRateXLbl.text = "1.5x"
            myPlayer.prepareToPlay()
            myPlayer.play()
        }
            
        else if sender.tag == 2 {
            sender.tag = 0
            myPlayer.stop()
            myPlayer.rate = 0.5
            playRateXLbl.text = "0.5x"
            myPlayer.prepareToPlay()
            myPlayer.play()
        }
            
        else if sender.tag == 0 {
            sender.tag = 1
            myPlayer.stop()
            myPlayer.rate = 1
            playRateXLbl.text = "1x"
            myPlayer.prepareToPlay()
            myPlayer.play()
        }
        
    }
    
}


//***********************************************
//MARK:-
//MARK:-   UITableView DataSource, Delegate
//***********************************************
//extension singleAnimalVC : UITableViewDelegate,UITableViewDataSource {
//
//}



//***********************************************
//MARK:-
//MARK:-   UICollectionView DataSource, Delegate
//***********************************************
//extension singleAnimalVC : UICollectionViewDelegate,UICollectionViewDataSource {
//
//}



//***********************************************
//MARK:-
//MARK:-   UITextField Delegate
//***********************************************
extension singleAnimalVC: UITextFieldDelegate {

}


//    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
//        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
//    }
