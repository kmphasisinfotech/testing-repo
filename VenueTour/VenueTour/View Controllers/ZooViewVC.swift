//
//  ZooViewVC.swift
//  VenueTour
//
//  Created by Mayank on 30/03/19.
//  Copyright © 2019 Mayank. All rights reserved.
//


import Foundation
import UIKit
import CarbonKit

class ZooViewVC: UIViewController {
    
    //***********************************************
    //MARK:-
    //MARK:-   Outlets
    //***********************************************
    
    @IBOutlet weak var zooNameLbl: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    
    //***********************************************
    //MARK:-
    //MARK:-   Other Properties
    //***********************************************
    
    var vcArr = [UIViewController]()
    
    
    //***********************************************
    //MARK:-
    //MARK:-  VC Life Cycle
    //***********************************************
    override func viewDidLoad() {
        super.viewDidLoad()
    
        print(zooNameLbl.font)
        
        vcArr = [UIStoryboard.instantiateVC(ZooParkVC.self), UIStoryboard.instantiateVC(AnimalsVC.self)]
        
        let carbonNav = CarbonTabSwipeNavigation(items: ["Zoo Park", "Animals"], delegate: self)
        carbonNav.insert(intoRootViewController: self, andTargetView: containerView)
        carbonNav.setIndicatorColor(UIColor.white)
        carbonNav.setIndicatorHeight(3)
        carbonNav.setNormalColor(UIColor("424C4E"), font: UIFont(name: "Open Sans", size: 18.0)!)
        carbonNav.setSelectedColor(UIColor.white, font: UIFont(name: "Open Sans", size: 18.0)!)
        carbonNav.setTabBarHeight(55)
        carbonNav.toolbar.barTintColor = UIColor("88B836")
        for i in 0...1 {
            carbonNav.carbonSegmentedControl!.setWidth(UIScreen.Width/2, forSegmentAt: i)
        }
    
    }
    
    
}



//***********************************************
//MARK:-
//MARK:-   IBActions
//***********************************************
extension ZooViewVC {
    
}


//***********************************************
//MARK:-
//MARK:-   UITableView DataSource, Delegate
//***********************************************
//extension ZooViewVC : UITableViewDelegate,UITableViewDataSource {
//
//}



//***********************************************
//MARK:-
//MARK:-   UICollectionView DataSource, Delegate
//***********************************************
//extension ZooViewVC : UICollectionViewDelegate,UICollectionViewDataSource {
//
//}



//***********************************************
//MARK:-
//MARK:-   UITextField Delegate
//***********************************************
extension ZooViewVC: UITextFieldDelegate {

}


//***********************************************
//MARK:-
//MARK:-   CarbonLit Delegate
//***********************************************
extension ZooViewVC : CarbonTabSwipeNavigationDelegate {
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        return vcArr[Int(index)]
    }
}
