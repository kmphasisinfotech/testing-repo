//
//  AnimalsVC.swift
//  VenueTour
//
//  Created by Mayank on 30/03/19.
//  Copyright © 2019 Mayank. All rights reserved.
//


import Foundation
import UIKit

class AnimalsVC: UIViewController {
    
    //***********************************************
    //MARK:-
    //MARK:-   Outlets
    //***********************************************
    
    @IBOutlet weak var tblView: UITableView!
    
    
    //***********************************************
    //MARK:-
    //MARK:-   Other Properties
    //***********************************************
    
    var imagArr = [UIImage(named: "Group 12"),UIImage(named: "Group 13"),UIImage(named: "Group 14"),UIImage(named: "Group 15")]
    var nameArr = ["Giant Anteater","Flamingo","Jaguar","Bear"]
    
    
    //***********************************************
    //MARK:-
    //MARK:-  VC Life Cycle
    //***********************************************
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}



//***********************************************
//MARK:-
//MARK:-   IBActions
//***********************************************
extension AnimalsVC {
    
}


//***********************************************
//MARK:-
//MARK:-   UITableView DataSource, Delegate
//***********************************************
extension AnimalsVC : UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return nameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeCell(AnimalCell.self, indexPath: indexPath)
        
        cell.animalImg.image = imagArr[indexPath.row]
        cell.animalNameLbl.text = nameArr[indexPath.row]
       
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = UIStoryboard.instantiateVC(singleAnimalVC.self)
        vc.animalName = nameArr[indexPath.row]
        self.show(vc, sender: nil)
    }
}



//***********************************************
//MARK:-
//MARK:-   UICollectionView DataSource, Delegate
//***********************************************
//extension AnimalsVC : UICollectionViewDelegate,UICollectionViewDataSource {
//
//}



//***********************************************
//MARK:-
//MARK:-   UITextField Delegate
//***********************************************
extension AnimalsVC: UITextFieldDelegate {

}


//***********************************************
//MARK:-
//MARK:-   UITableView Cell
//***********************************************
class AnimalCell: UITableViewCell {
    
    @IBOutlet weak var completeView: UIView!
    @IBOutlet weak var animalImg: UIImageView!
    @IBOutlet weak var animalNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
