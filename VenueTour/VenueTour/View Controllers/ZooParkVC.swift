//
//  ZooParkVC.swift
//  VenueTour
//
//  Created by Mayank on 30/03/19.
//  Copyright © 2019 Mayank. All rights reserved.
//


import Foundation
import UIKit

class ZooParkVC: UIViewController {
    
    //***********************************************
    //MARK:-
    //MARK:-   Outlets
    //***********************************************
    
    
    
    //***********************************************
    //MARK:-
    //MARK:-   Other Properties
    //***********************************************
    
    
    
    
    //***********************************************
    //MARK:-
    //MARK:-  VC Life Cycle
    //***********************************************
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}



//***********************************************
//MARK:-
//MARK:-   IBActions
//***********************************************
extension ZooParkVC {
    
}


//***********************************************
//MARK:-
//MARK:-   UITableView DataSource, Delegate
//***********************************************
//extension ZooParkVC : UITableViewDelegate,UITableViewDataSource {
//
//}



//***********************************************
//MARK:-
//MARK:-   UICollectionView DataSource, Delegate
//***********************************************
//extension ZooParkVC : UICollectionViewDelegate,UICollectionViewDataSource {
//
//}



//***********************************************
//MARK:-
//MARK:-   UITextField Delegate
//***********************************************
extension ZooParkVC: UITextFieldDelegate {

}


