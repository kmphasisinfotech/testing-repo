//
//  AFClass.swift
//  DiNGr
//
//  Created by macOs on 20/01/18.
//  Copyright © 2018 apple. All rights reserved.
//

import Foundation
import AFNetworking
import SVProgressHUD
import SystemConfiguration


let NetworkError = "We're having trouble reaching the network. Check your connection or try again in a few minutes."


public typealias APICallback = ( _ response: Any?, _ responsemessage: String?,_ resCode: Int?, _ errorStr: String?) -> Void

protocol Mappable {
    init?(dict: [String: Any])
}

extension Mappable {
    func getIdStr() -> String { return ""}
}

class AFClass {
    
    private static let keyToken = "6ecf1a4f840ec8905a70c66c37m3512z"
    
    class func postRequest<T: Mappable>(with urlStr: String, params: [String:Any], headerKeys: [String], isKey: Bool = true, entity: T.Type, isObject: Bool? = nil, key: String = "data", isShowHud: Bool = true, completion: @escaping APICallback) {
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        
        guard Reachability.isConnectedToNetwork() else {
            completion(nil, nil,nil,NetworkError)
            return
        }
        
        headerKeys.forEach { (keyStr) in
            
            if let token = UserDefaults.standard.value(forKey: keyStr) as? String {
                manager.requestSerializer.setValue(token, forHTTPHeaderField: keyStr)
            }
        }
        
        if isKey {
            manager.requestSerializer.setValue(keyToken, forHTTPHeaderField: "key")
        }
        
        var pams : [String:Any] = [:]
        
        let finalParam = AFShared.getTrimmedParams(params)
        if finalParam.count > 0 {
            pams = finalParam
        }
        
        print(pams)
        
        
        if isShowHud {
            SVProgressHUD.setDefaultMaskType(.clear)
            SVProgressHUD.show(withStatus: "Loading\nPlease Wait...")
        }
        
        manager.post(urlStr, parameters: pams, progress: { (progress) in
            
        }, success: { (task, response) in
            
            if isShowHud {
                SVProgressHUD.dismiss()
            }
            
            if (task.response as! HTTPURLResponse).statusCode >= 200 && (task.response as! HTTPURLResponse).statusCode <= 299 {

//                let str = String(data: response as! Data, encoding: String.Encoding.utf8)
                
                let json = (try? JSONSerialization.jsonObject(with: response as! Data, options: [])) as? [String:Any]
              
                if let res = json{
                    
                    print(res)
                    
                    let msg = res["ResponseMsg"] as? String ?? ""
                    let code = res["ResponseCode"] as? Int ?? -1
                    
                    if code != 0 {
                        if let isObject = isObject{
                            if isObject {
                                if let objectDic = res[key] as? [String:Any] {
                                    if let entity = T.init(dict: objectDic) {
                                        completion(entity as Any?, msg,code,nil)
                                        return
                                    }
                                }
                                else {
                                    completion(nil, msg,code,msg)
                                    return
                                }
                            } else {
                                if let objectDicArray = res[key] as? [[String: Any]] {
                                    var objectsArray = [T]()
                                    for objectDic in objectDicArray {
                                        if let entity = T.init(dict: objectDic) {
                                            objectsArray.append(entity)
                                        }
                                    }
                                    completion(objectsArray as Any?, msg,code,nil)
                                    return
                                }
                            }
                            
                            completion(nil, msg,code,task.error?.localizedDescription)
                        } else {
                            completion(res,msg,code,nil)
                            return
                        }
                    }
                    else {
                        completion(nil, nil,code,msg)
                    }
                }
            }
            
        }) { (sessiontask, error) in
            
            if isShowHud {
                SVProgressHUD.dismiss()
            }
            
            print(error)
            if let code = (sessiontask?.response as? HTTPURLResponse)?.statusCode {
                completion(nil, nil,code,error.localizedDescription)
            }
            else {
                completion(nil, nil,nil,error.localizedDescription)
            }
            
        }
    }
    
    class func postRequest<T: Mappable>(with urlStr: String, params: [String:Any], headerKeys: [String], isKey: Bool = true, entity: T.Type, isObject: Bool? = nil, key: String = "data",  images: [[UIImage]], imageNames: [String], isShowHud: Bool = true, completion: @escaping APICallback) {
        
        let manager = AFHTTPSessionManager()
        
        guard Reachability.isConnectedToNetwork() else {
            completion(nil, nil,nil,NetworkError)
            return
        }
        
        headerKeys.forEach { (keyStr) in
            
            let token = UserDefaults.standard.value(forKey: keyStr) as! String
            manager.requestSerializer.setValue(token, forHTTPHeaderField: keyStr)
        }
        
        if isKey {
            manager.requestSerializer.setValue(keyToken, forHTTPHeaderField: "key")
        }
        
        let finalParam = AFShared.getTrimmedParams(params)
        
        if isShowHud {
            SVProgressHUD.show(withStatus: "Loading\nPlease Wait...")
        }
        
        manager.post(urlStr, parameters: finalParam, constructingBodyWith: { (formdata) in
            
            if images.count == imageNames.count {
                
                for (nameInd, name) in imageNames.enumerated() {
                    
                    print(images[nameInd])
                    
                    for img in images[nameInd] {
                        if let data = img.jpegData(compressionQuality: 1.0) {
                            let uniqueStr = ProcessInfo.processInfo.globallyUniqueString.appending(".jpg")
                            formdata.appendPart(withFileData: data, name: name, fileName: uniqueStr, mimeType: "image/jpeg")
                        }
                    }
                }
            }
            
            
        }, progress: { (progress) in
            
            print(progress)
            
        }, success: { (task, response) in
            
            if isShowHud {
                SVProgressHUD.dismiss()
            }
            
            if (task.response as! HTTPURLResponse).statusCode >= 200 && (task.response as! HTTPURLResponse).statusCode <= 299 {
                
                if let res = response as? [String : Any]{
                  
                    print(res)
                    
                    let msg = res["ResponseMsg"] as? String ?? ""
                    let code = res["ResponseCode"] as? Int ?? -1
                    
                    if code != 0 {
                        if let isObject = isObject{
                            if isObject {
                                if let objectDic = res[key] as? [String:Any] {
                                    if let entity = T.init(dict: objectDic) {
                                        completion(entity as Any?, msg,code,nil)
                                        return
                                    }
                                }
                            } else {
                                if let objectDicArray = res[key] as? [[String: Any]] {
                                    var objectsArray = [T]()
                                    for objectDic in objectDicArray {
                                        if let entity = T.init(dict: objectDic) {
                                            objectsArray.append(entity)
                                        }
                                    }
                                    completion(objectsArray as Any?, msg,code,nil)
                                    return
                                }
                            }
                            completion(nil, msg,code,task.error?.localizedDescription)
                        } else {
                            completion(res,msg,code,nil)
                            return
                        }
                    }
                    else {
                        completion(nil, nil,code,msg)
                    }
                }
            }
            
        }) { (sessiontask, error) in
            
            if isShowHud {
                SVProgressHUD.dismiss()
            }
            
            print(error)
            if let code = (sessiontask?.response as? HTTPURLResponse)?.statusCode {
               completion(nil, nil,code,error.localizedDescription)
            }
            else {
                completion(nil, nil,nil,error.localizedDescription)
            }
            
        }
    }
 /*
    class func getRequest<T: Mappable>(with urlStr: String, params: [String:Any]? = nil, headerKeys: [String], isKey: Bool = true, entity: T.Type, isObject: Bool? = nil, key: String = "data", isShowHud: Bool = true, completion: @escaping APICallback) {
        
        
        guard AFNetworkReachabilityManager.shared().isReachable else {
            completion(nil, nil,nil,NetworkError)
            return
        }
        
        let manager = AFHTTPSessionManager()
//        manager.requestSerializer = AFHTTPRequestSerializer()
        
        headerKeys.forEach { (keyStr) in
            manager.requestSerializer.setValue("Basic \(keyStr)", forHTTPHeaderField: "Authorization")
        }
        
        if isKey {
            manager.requestSerializer.setValue(keyToken, forHTTPHeaderField: "key")
        }
        
        var finalParam = [String:Any]()
        if params != nil {
            finalParam = AFShared.getTrimmedParams(params!)
        }
        
        if isShowHud {
            
            if !SVProgressHUD.isVisible() {
                SVProgressHUD.show(withStatus: "Loading\nPlease Wait...")
            }
        }
        
        manager.get(urlStr, parameters: finalParam, progress: { (progress) in
            
        },success: { (task, response) in
            
            if isShowHud {
                SVProgressHUD.dismiss()
            }
            
            
            if (task.response as! HTTPURLResponse).statusCode >= 200 && (task.response as! HTTPURLResponse).statusCode <= 299 {
                
                if let res = response as? [String : Any]{
                    print(res)
                    
                    if let responseArr = res["response"] as? [[String:Any]] {
                        
                        if responseArr.count > 0 {
                            
                            if let status = responseArr[0]["Status"] as? String {
                                
                                if status == "Success" {
                                    
                                    if let isObject = isObject{
                                        if isObject {
                                            if let objectDic = responseArr[0][key] as? [String:Any] {
                                                if let entity = T.init(dict: objectDic) {
                                                    completion(entity as Any?, nil, nil,nil)
                                                    return
                                                }
                                            }
                                        } else {
                                            if let objectDicArray = responseArr[0][key] as? [[String: Any]] {
                                                var objectsArray = [T]()
                                                for objectDic in objectDicArray {
                                                    if let entity = T.init(dict: objectDic) {
                                                        objectsArray.append(entity)
                                                    }
                                                }
                                                completion(objectsArray as Any?, nil, nil,nil)
                                                return
                                            }
                                        }
                                        completion(nil, nil,nil, task.error?.localizedDescription)
                                    } else {
                                        completion(response, nil,nil,nil)
                                        return
                                    }
                                }
                                else {
                                    completion(nil, nil,code,responseArr[0]["Text_Message"] as? String ?? "")
                                }
                            }
                        }
                    }
                    else {
                        completion(response, nil, nil)
                    }
                }
            }
            else {
                
            }
            
        }) { (sessiontask, error) in
            
            if isShowHud {
                SVProgressHUD.dismiss()
            }
            
            print(error)
            completion(nil, nil, error.localizedDescription)
        }
        
    }
 
 */
 
}

fileprivate class AFShared {
    
    static var shared : AFHTTPSessionManager {
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.reachabilityManager.startMonitoring()
        return manager
    }
    
    
    class func getTrimmedParams(_ params: [String:Any]) -> [String:Any]{
        
        var trimedParam = [String:Any]()
        
        for (keyP, value) in params {
            if let str = value as? String {
                let trimmed = str.trimmingCharacters(in: CharacterSet.whitespaces)
                trimedParam[keyP] = trimmed
            }
            else {
                trimedParam[keyP] = value
            }
        }
        
        return trimedParam
    }
    
    class func setHeaderKey(_ headers: [String]) {
        
        headers.forEach { (keyStr) in
            AFShared.shared.requestSerializer.setValue("Basic "+keyStr, forHTTPHeaderField: "Authorization")
        }
    }
    
}



open class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}

